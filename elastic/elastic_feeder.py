#!/usr/bin/env python
# -*- coding: utf-8 -*-
from elasticsearch import Elasticsearch
from datetime import datetime
import requests
import json
import time
import random

deviceIp = ["10.205.120.140","109.78.112.104","79.89.112.30","14.152.110.3","178.19.92.110"]
physicalPort = ["33213","55543","11123","49438","39743"]
policyName = ["BDOS","Bdos6","bdos7","Di1017","Di11030846","acme_policy1"]
direction = ["Inbound","Outbound"]
trafficValue = ["0"]
discards = ["0"]
unit = ["bps","pps"]
monitoringProtocol = ["sctp","tcp","icmp","all","igmp","udp","other"]

es = Elasticsearch([{'host':'0.0.0.0','port':9200}], http_auth=('elastic', 'changeme'),)

es.indices.delete(index='dp-traffic-raw', ignore=[400, 404])

request_body = {
  "settings" : {
    "number_of_shards": 5,
    "number_of_replicas": 1
  },
  "mappings": {
    "events" :{
      "properties" : {
        'deviceIp': {'index': 'analyzed', 'type': 'ip'},
        'physicalPort': {'index': 'analyzed', 'type': 'double'},
        'policyName': {'index': 'analyzed', 'type': 'string'},
        'direction': {'index': 'not_analyzed', 'type': 'string'},
        'trafficValue': {'index': 'analyzed', 'type': 'double'},
        'discards': {'index': 'not_analyzed', 'type': 'double'},
        'unit': {'index': 'analyzed', 'type': 'string'},
        'monitoringProtocol': {'index': 'analyzed', 'type': 'string'},
        'timeStamp': {'index': 'analyzed', 'type': 'date'}
      }
    }
  }
}

es.indices.create(index='dp-traffic-raw',body=request_body)
elastic = requests.get("http://0.0.0.0:9200")
i = 1
while elastic.status_code == 200 or elastic.status_code == 401:
        try:
                now = datetime.utcnow()
                ltime = now.strftime("%Y-%m-%dT%H:%M:%S") + ".%03d" % (now.microsecond / 1000) + "Z"
                monitor_object = {}
                monitor_object['deviceIp'] = random.choice(deviceIp)
                monitor_object['physicalPort'] = random.choice(physicalPort)
                monitor_object['policyName'] = random.choice(policyName)
                monitor_object['direction'] = random.choice(direction)
                monitor_object['trafficValue'] = random.choice(trafficValue)
                monitor_object['discards'] = random.choice(discards)
                monitor_object['unit'] = random.choice(unit)
                monitor_object['monitoringProtocol'] = random.choice(monitoringProtocol)
                monitor_object['timeStamp'] = ltime
                es.index(index='dp-traffic-raw', doc_type='traffic-raw',id=i, body=monitor_object)
                print es.get(index='dp-traffic-raw', doc_type='traffic-raw', id=i)
                i = i+1
        except Exception as e:
                print e

        time.sleep(5)

