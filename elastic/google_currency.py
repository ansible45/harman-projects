#!/usr/bin/env python
# -*- coding: utf-8 -*-
from elasticsearch import Elasticsearch
from datetime import datetime
from bs4 import BeautifulSoup
import requests
import json
import time
import random
import re


def google_currency(amount,from_currenct,to_currency):
  currency_url = "http://finance.google.com/finance/converter?a=%s&from=%s&to=%s" % (amount,from_currenct,to_currency)
  raw_response = requests.get(currency_url)
  soup = BeautifulSoup(raw_response.text, 'html.parser')
  res = soup.find('span', {'class':'bld'})
  return re.findall(r'\d+\.\d+', res.text)[0]

def delete_index(host,port,user,password,index):
  es = Elasticsearch([{'host':host,'port':port}], http_auth=(user,password),)
  es.indices.delete(index=index, ignore=[400, 404])

def elastic_feeder(host,port,user,password,index,currency,doc_type,object_id):
  es = Elasticsearch([{'host':host,'port':port}], http_auth=(user,password),)

  exist = es.indices.exists(index)
  if not(exist):
    es = Elasticsearch([{'host':host,'port':port}], http_auth=(user,password),)
    request_body = {
      "settings" : {
        "number_of_shards": 5,
        "number_of_replicas": 1
      },
      "mappings": {
        "events" :{
          "properties" : {
            'currency': {'index': 'analyzed', 'type': 'double'},
            'timeStamp': {'index': 'analyzed', 'type': 'date'}
          }
        }
      }
    }
    es.indices.create(index=index,body=request_body)

  try:
      now = datetime.utcnow()
      ltime = now.strftime("%Y-%m-%dT%H:%M:%S") + ".%03d" % (now.microsecond / 1000) + "Z"
      monitor_object = {}
      monitor_object['currency'] = currency
      monitor_object['timeStamp'] = ltime
      es.index(index=index, doc_type=doc_type ,id=object_id, body=monitor_object)
      return es.get(index=index, doc_type=doc_type, id=object_id)
  except Exception as e:
        print e

object_id = 0

while True:
  price = google_currency(1,"BTC","USD")
  elastic_feeder('0.0.0.0','9200','elastic','changeme','btc-usd',price,'currency',object_id)
  
  price = google_currency(1,"EUR","USD")
  elastic_feeder('0.0.0.0','9200','elastic','changeme','eur-usd',price,'currency',object_id)

  object_id+=1
  time.sleep(5)





