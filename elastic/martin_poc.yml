---
- name: "Install and configure elk"
  hosts: elk
  environment:
    PYTHONPATH: "{{ lookup('env','PYTHONPATH') }}:/usr/local/lib/python2.7/dist-packages:/usr/local/lib/python2.7/site-packages"
  vars: 
    docker_yum_repo: "https://download.docker.com/linux/centos/7/$basearch/stable"
    docker_yum_repo_gpg: 'https://download.docker.com/linux/centos/gpg'
    docker_minimal_kernel_version: "3.10"
    docker_group_id: 797
  tasks:
    - shell: echo {{ansible_env.HOME}}
      register: home
    - debug: msg="root home - {{ home.stdout }}"

    - name: Create elk group
      become: yes
      group:
        name: elk
        state: present

    - name: Create elk user
      become: yes
      user:
        name: elk
        comment: "elk server"
        group: elk

    - name: Check kernel version for docker support
      fail:
        msg: "Kernel version {{ ansible_kernel }} does not meet minimum version ({{ docker_minimal_kernel_version }})."
      when: "ansible_kernel | version_compare(docker_minimal_kernel_version, '<')"

    - name: Create docker group
      become: yes
      group:
        name: docker
        state: present
        gid: '{{ docker_group_id }}'
        system: yes

    - name: Ensure older Docker packages are removed (pre docker-CE)
      become: yes
      package:
        name: '{{ item }}'
        state: absent
      with_items:
        - docker-engine
        - docker
      register: dockercleaned

    - name: Remove old systemd drop-in file
      become: yes
      file:
        path: '/etc/systemd/system/docker.service.d/override.conf'
        state: absent
      when: dockercleaned.changed
      tags: skip_ansible_lint

    - name: add the docker ce yum repo
      become: yes
      yum_repository:
        name: "docker-ce"
        description: "Docker CE YUM repo"
        gpgcheck: "yes"
        enabled: "yes"
        baseurl: "{{docker_yum_repo}}"
        gpgkey: "{{docker_yum_repo_gpg}}"

    - name: Install EPEL repo.
      become: yes
      yum:
        name: https://dl.fedoraproject.org/pub/epel/epel-release-latest-{{ ansible_distribution_major_version }}.noarch.rpm
        state: present

    - name: Import EPEL GPG key.
      become: yes
      rpm_key:
        key: /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-{{ ansible_distribution_major_version }}
        state: present

    - name: Install remi repo.
      become: yes
      yum:
        name: http://rpms.famillecollet.com/enterprise/remi-release-{{ ansible_distribution_major_version }}.rpm
        state: present

    - name: Import remi GPG key.
      become: yes
      rpm_key:
        key: http://rpms.remirepo.net/RPM-GPG-KEY-remi
        state: present

    - name: install the prerequisites using yum
      become: yes
      yum:
        update_cache: yes
        name: "{{ item }}"
        state: "latest"
      with_items:
        - "epel-release"
        - "libselinux-python"
        - "git"
        - "python"
        - "python-pip"
        - "docker-ce"

    - name: install using pip
      become: yes
      pip:
        name: "{{ item }}"
      with_items:
        - docker-py
        - elasticsearch
        - gdata
        - bs4
        - csvmapper

    - name: stop and enable docker
      become: yes
      systemd:
        name: "docker"
        enabled: "yes"
        state: "stopped"

    - name: Edit docker deamon file and change storage driver to devicemapper
      become: yes
      lineinfile:
        path: /etc/docker/daemon.json
        line: '{ "storage-driver": "devicemapper" }'
        create: yes

    - name: start and enable docker
      become: yes
      systemd:
        name: "docker"
        enabled: "yes"
        state: "started"

    - name: Create elastic directory/volume
      become: yes
      file: path=/mnt/elastic state=directory

    - name: Create kibana directory/volume
      become: yes
      file: path=/mnt/kibana state=directory

    - name: copy files to VM
      become: yes
      copy:
        src: "{{ item }}"
        dest: "{{ansible_env.HOME}}/{{ item }}"
        owner: elk
        group: elk
        mode: 0644
      with_items:
        - elastic_feeder.py
        - logstash.conf
        - google_currency.py
        - earthquake_feeder.py
        - earthquakes.csv
        - stocks.csv
        - stock_feeder.py

    - name: remove elastic application container if exists
      become: yes
      docker_container:
        name: elastic
        force_kill: true
        state: absent

    - name: remove kibana application container if exists
      become: yes
      docker_container:
        name: kibana
        force_kill: true
        state: absent

    - name: remove jpetstore application container if exists
      become: yes
      docker_container:
        name: jpetstore
        force_kill: true
        state: absent

    - name: elastic application container
      become: yes
      docker_container:
        name: elastic
        image: elasticsearch
        state: started
        pull: yes
        privileged: true
        detach: true
        interactive: true
        restart_policy: always
        env:
          IMPORT_FROM_VOLUME: true
        volumes:
          - /mnt/elastic:/usr/share/elasticsearch/data
        exposed_ports:
          - 9200
          - 9300
        published_ports:
          - 9200:9200
          - 9300:9300

    - name: Wait for ELASTIC to start on port 9200
      uri:
        url: "http://127.0.0.1:9200"
        status_code: 200
      register: result
      until: result.status == 200
      retries: 1000
      delay: 10

    - name: kibana application container
      become: yes
      docker_container:
        name: kibana
        image: kibana
        state: started
        pull: yes
        privileged: true
        detach: true
        interactive: true
        restart_policy: always
        links:
          - "elastic:elasticsearch"
        env:
          IMPORT_FROM_VOLUME: true
        exposed_ports:
          - 5601
        published_ports:
          - 80:5601

    - name: Wait for KIBANA to start on port 80
      uri:
        url: "http://127.0.0.1:80"
        status_code: 200
      register: result
      until: result.status == 200
      retries: 1000
      delay: 10

    - name: jpetstore application container
      become: yes
      docker_container:
        name: jpetstore
        image: camillegr/jpetstore
        state: started
        pull: yes
        privileged: true
        detach: true
        interactive: true
        restart_policy: always
        links:
          - "elastic:elasticsearch"
        env:
          IMPORT_FROM_VOLUME: true
        exposed_ports:
          - 8080
        published_ports:
          - 8080:8080

    - name: Wait for jpetstore to start on port 8080
      uri:
        url: "http://127.0.0.1:8080"
        status_code: 200
      register: result
      until: result.status == 200
      retries: 1000
      delay: 10
   
    - name: run x-pack installation in container elastic
      become: yes
      command: docker exec elastic bin/elasticsearch-plugin install x-pack
      register: result_of_action_elastic
    - debug: msg="{{ result_of_action_elastic.stdout_lines }}"

    - name: restart elastic container
      become: yes
      command: docker restart elastic

    - name: run x-pack installation in container kibana
      become: yes
      command: docker exec kibana /usr/share/kibana/bin/kibana-plugin install x-pack
      register: result_of_action_kibana
    - debug: msg="{{ result_of_action_kibana.stdout_lines }}"

    - name: restart kibana container
      become: yes
      command: docker restart kibana

    - name: download logstash on application
      become: yes
      command: docker exec jpetstore wget -O /tmp/logstash-2.4.0.tar.gz 'https://download.elastic.co/logstash/logstash/logstash-2.4.0.tar.gz'
      register: result_of_action
    - debug: msg="{{ result_of_action.stdout_lines }}"

    - name: deploy logstash on application
      become: yes
      command: docker exec jpetstore tar -xzf /tmp/logstash-2.4.0.tar.gz -C /tmp
      register: result_of_action
    - debug: msg="{{ result_of_action.stdout_lines }}"

    - name: copy logstash.conf to application 
      become: yes
      command: docker cp {{ansible_env.HOME}}/logstash.conf jpetstore:/tmp/logstash.conf 
      register: result_of_action
    - debug: msg="{{ result_of_action.stdout_lines }}"

    - name: start logstash process on application
      become: yes
      command: docker exec -dit jpetstore /tmp/logstash-2.4.0/bin/logstash -f /tmp/logstash.conf
      register: result_of_action
    - debug: msg="{{ result_of_action.stdout_lines }}"




    
