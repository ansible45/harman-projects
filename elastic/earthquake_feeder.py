#!/usr/bin/env python
# -*- coding: utf-8 -*-
from elasticsearch import Elasticsearch
from datetime import datetime
from bs4 import BeautifulSoup
import requests
import csv
import json
import time
import random
import re
import csvmapper

def csv_convert(filepath,fields):
  parser = csvmapper.CSVParser(filepath, csvmapper.FieldMapper(fields))
  converter = csvmapper.JSONConverter(parser)
  return converter.doConvert(pretty=True)


def delete_index(host,port,user,password,index):
  es = Elasticsearch([{'host':host,'port':port}], http_auth=(user,password),)
  es.indices.delete(index=index, ignore=[400, 404])

def elastic_feeder(host,port,user,password,index,data,doc_type,object_id):
  es = Elasticsearch([{'host':host,'port':port}], http_auth=(user,password),)

  exist = es.indices.exists(index)
  if not(exist):
    es = Elasticsearch([{'host':host,'port':port}], http_auth=(user,password),)
    request_body = {
      "settings" : {
        "number_of_shards": 5,
        "number_of_replicas": 1
      },
      "mappings": {
        "events" :{
          "properties" : {
            'Millisecond': {'index': 'analyzed', 'type': 'double'},
            'Md': {'index': 'analyzed', 'type': 'double'},
            'Mb': {'index': 'analyzed', 'type': 'double'},
            'Mw': {'index': 'analyzed', 'type': 'double'},
            'Depth(Km)': {'index': 'analyzed', 'type': 'double'},
            'Region': {'index': 'analyzed', 'type': 'string'},
            'Type': {'index': 'not_analyzed', 'type': 'string'},
            'location': {'index': 'analyzed', 'type': 'geo_point'},
            'timeStamp': {'index': 'analyzed', 'type': 'date'}
          }
        }
      }
    }
    es.indices.create(index=index,body=request_body)

  try:
    es.index(index=index, doc_type=doc_type ,id=object_id, body=data)
    return es.get(index=index, doc_type=doc_type, id=object_id)
  except Exception as e:
    print object_id
    print e

fields = ('Date', 'Time(UTC)', 'Millisecond', 'Md',	'Mb',	'Mw',	'Lat'	,'Long',	'Depth(Km)',	'Region',	'Type')
for i,single in enumerate(json.loads(csv_convert('earthquakes.csv',fields))):
  if i>0:
    print i
    data_object = {}
    data_object['Millisecond'] = single['Millisecond']
    data_object['Md'] = single['Md']
    data_object['Mb'] = single['Mb']
    data_object['Mw'] = single['Mw']
    data_object['Depth(Km)'] = single['Depth(Km)']
    data_object['Region'] = single['Region']
    data_object['Type'] = single['Type']
    data_object['location'] = single['Lat']+","+single['Long']
    data_object['timeStamp'] = single['Date']+"T"+single['Time(UTC)']+"Z"
    elastic_feeder('0.0.0.0','9200','elastic','changeme','israel-earthquakes',data_object,'earthquakes',i)





