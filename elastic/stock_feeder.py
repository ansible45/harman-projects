#!/usr/bin/env python
# -*- coding: utf-8 -*-
from elasticsearch import Elasticsearch
from datetime import datetime
from bs4 import BeautifulSoup
import requests
import csv
import json
import time
import random
import re
import csvmapper

def csv_convert(filepath,fields):
  parser = csvmapper.CSVParser(filepath, csvmapper.FieldMapper(fields))
  converter = csvmapper.JSONConverter(parser)
  return converter.doConvert(pretty=True)


def delete_index(host,port,user,password,index):
  es = Elasticsearch([{'host':host,'port':port}], http_auth=(user,password),)
  es.indices.delete(index=index, ignore=[400, 404])

def elastic_feeder(host,port,user,password,index,data,doc_type,object_id):
  es = Elasticsearch([{'host':host,'port':port}], http_auth=(user,password),)

  exist = es.indices.exists(index)
  if not(exist):
    es = Elasticsearch([{'host':host,'port':port}], http_auth=(user,password),)
    request_body = {
      "settings" : {
        "number_of_shards": 5,
        "number_of_replicas": 1
      },
      "mappings": {
        "events" :{
          "properties" : {
            'Base': {'index': 'analyzed', 'type': 'double'},
            'Open': {'index': 'analyzed', 'type': 'double'},
            'Close': {'index': 'analyzed', 'type': 'double'},
            'High': {'index': 'analyzed', 'type': 'double'},
            'Low': {'index': 'analyzed', 'type': 'double'},
            'Market-value': {'index': 'analyzed', 'type': 'double'},
            'timeStamp': {'index': 'analyzed', 'type': 'date'}
          }
        }
      }
    }
    es.indices.create(index=index,body=request_body)

  try:
    es.index(index=index, doc_type=doc_type ,id=object_id, body=data)
    return es.get(index=index, doc_type=doc_type, id=object_id)
  except Exception as e:
    print object_id
    print e

fields = ('DATE', 'BASE', 'OPEN', 'CLOSE',	'HIGH',	'LOW',	'MARKET VALUE')
for i,single in enumerate(json.loads(csv_convert('stocks.csv',fields))):
  if i>0:
    print i
    data_object = {}
    data_object['Base'] = single['BASE']
    data_object['Open'] = single['OPEN']
    data_object['Close'] = single['CLOSE']
    data_object['High'] = single['HIGH']
    data_object['Low'] = single['LOW']
    data_object['Market-value'] = single['MARKET VALUE']
    data_object['timeStamp'] = datetime.strptime(single['DATE'], '%d/%m/%Y').strftime('%Y-%m-%d')+"T00:00:00Z"
    elastic_feeder('0.0.0.0','9200','elastic','changeme','israel-stocks',data_object,'stocks',i)






