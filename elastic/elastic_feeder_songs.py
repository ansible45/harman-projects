#!/usr/bin/env python
# -*- coding: utf-8 -*-
from elasticsearch import Elasticsearch
from datetime import datetime
import requests
import json
import time
import random

users = ["10.205.120.140","109.78.112.104","79.89.112.30","14.152.110.3","178.19.92.110","118.19.920.110","56.11.32.128","43.23.124.152"]
songs = {"classical" : [
          {"artist":"Beethoven","song":"Symphony No. 5: I"},
          {"artist":"Tchaikovsky","song":"1812 Overture"},
          {"artist":"Mozart","song":"Eine Kleine Nachtmusik: Allegro"},
          {"artist":"Bach","song":"Toccata And Fugue In D Minor"},
          {"artist":"Beethoven","song":"Moonlight Sonata"},
          {"artist":"Tchaikovsky","song":"Nutcracker: Dance Of The Sugar-Plum Fairy"},
          {"artist":"Bach","song":"Air On The G String"},
          {"artist":"Mozart","song":"Magic Flute: Queen Of The Night"},
          {"artist":"Mozart","song":"Marriage Of Figaro Overture"},
          {"artist":"Beethoven","song":"Minuet In G"},
          {"artist":"Tchaikovsky","song":"Nutcracker: Dance Of The Mirlitons"},
          {"artist":"Bach","song":"Cello Suite No. 1"},
          {"artist":"Bach","song":"Minuet In G"},
          {"artist":"Mozart","song":"Minuet In G"},
          {"artist":"Beethoven","song":"Symphony No. 9: II"},
          {"artist":"Mozart","song":"Piano Sonata No. 16"},
          {"artist":"Bach","song":"Brandenburg Concerto No. 3: I"},
          {"artist":"Mozart","song":"Requiem: Dies Irae"},
          {"artist":"Mozart","song":"Symphony No. 40: I"},
          {"artist":"Beethoven","song":"Symphony No. 7: II"},
          {"artist":"Mozart","song":"Magic Flute Overture"},
          {"artist":"Chopin","song":"Funeral March"},
          {"artist":"Vivaldi","song":"Four Seasons: Spring"},
          {"artist":"Rossini","song":"Barber Of Seville: Largo Al Factotum"},
          {"artist":"Brahms","song":"Lullaby"},
          {"artist":"Vivaldi","song":"Four Seasons: Winter"},
          {"artist":"Tchaikovsky","song":"Piano Concerto No. 1"}
        ],
        "rock" : [
          {"artist":"Led Zeppelin","song":"Stairway to Heaven"},
          {"artist":"Queen","song":"Bohemian Rhapsody"},
          {"artist":"The Eagles","song":"Hotel California"},
          {"artist":"Lynyrd Skynyrd","song":"Free Bird"},
          {"artist":"The Who","song":"Baba O'Riley"},
          {"artist":"Pink Floyd","song":"Comfortably Numb"},
          {"artist":"Kansas","song":"Carry on Wayward Son"},
          {"artist":"The Beatles","song":"A Day in the Life"},
          {"artist":"The Beatles","song":"Hey Jude"},
          {"artist":"Led Zeppelin","song":"The Battle of Evermore"},
          {"artist":"Pink Floyd","song":"Wish You Were Here"},
          {"artist":"Pink Floyd","song":"Time"},
          {"artist":"The Beatles","song":"Let It Be"},
          {"artist":"Led Zeppelin","song":"Kashmir"},
          {"artist":"Kansas","song":"Song for America"},
          {"artist":"Led Zeppelin","song":"Over the Hills and Far Away"},
          {"artist":"Queen","song":"Somebody to Love"},
          {"artist":"The Beatles","song":"Yesterday"},
          {"artist":"Queen","song":"Another One Bites the Dust"},
          {"artist":"Led Zeppelin","song":"Going to California"},
          {"artist":"The Beatles","song":"Help!"},
          {"artist":"Lynyrd Skynyrd","song":"Simple Man"},
          {"artist":"Pink Floyd","song":"Dogs"}
      ]}

es = Elasticsearch([{'host':'0.0.0.0','port':9200}], http_auth=('elastic', 'changeme'),)

es.indices.delete(index='music-traffic-raw', ignore=[400, 404])

request_body = {
  "settings" : {
    "number_of_shards": 5,
    "number_of_replicas": 1
  },
  "mappings": {
    "events" :{
      "properties" : {
        'users': {'index': 'analyzed', 'type': 'ip'},
        'genre': {'index': 'analyzed', 'type': 'string'},
        'artist': {'index': 'analyzed', 'type': 'string'},
        'song': {'index': 'analyzed', 'type': 'string'},
        'timeStamp': {'index': 'analyzed', 'type': 'date'}
      }
    }
  }
}

es.indices.create(index='music-traffic-raw',body=request_body)
elastic = requests.get("http://0.0.0.0:9200")
i = 1
while elastic.status_code == 200 or elastic.status_code == 401:
        try:
                now = datetime.utcnow()
                ltime = now.strftime("%Y-%m-%dT%H:%M:%S") + ".%03d" % (now.microsecond / 1000) + "Z"
                monitor_object = {}
                genre = random.choice(["classical","rock"])
                song = random.choice(songs[genre])
          
                monitor_object['users'] = random.choice(users)
                monitor_object['song'] = song["song"]
                monitor_object['artist'] = song["artist"]
                monitor_object['genre'] = genre
                monitor_object['timeStamp'] = ltime
                es.index(index='music-traffic-raw', doc_type='music-type',id=i, body=monitor_object)
                print es.get(index='music-traffic-raw', doc_type='music-type', id=i)
                i = i+1
        except Exception as e:
                print e

        time.sleep(5)

