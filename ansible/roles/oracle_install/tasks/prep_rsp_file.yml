---
 
- name: make a copy of the db_install response file
  become: yes
  copy:
    src: "{{ oracle_tmp }}/database/response/db_install.rsp"
    dest: "{{ oracle_tmp }}/db_install.rsp"
    owner: "{{ oracle_user[0].name }}"
    group: "{{ oracle_group[0].name }}"
    remote_src: yes

- name: make a copy of the netca response file
  become: yes
  copy:
    src: "{{ oracle_tmp }}/database/response/netca.rsp"
    dest: "{{ oracle_tmp }}/netca.rsp"
    owner: "{{ oracle_user[0].name }}"
    group: "{{ oracle_group[0].name }}"
    remote_src: yes

- name: set inline changes for netca.rsp (common)
  become: yes
  lineinfile: 
    path: "{{ oracle_tmp }}/netca.rsp"
    regexp: "^{{ item.regex }}"
    line: "{{ item.regex }}={{ item.value }}"
  with_items:
    - { regex: CREATE_TYPE, value: "\"CUSTOM\"" }
    - { regex: INSTALL_TYPE , value: "\"CUSTOM\"" }
    - { regex: LISTENER_NUMBER , value: "1" }
    - { regex: LISTENER_NAMES , value: "{\"LISTENER_ORACLEDB1\"}" }
    - { regex: LISTENER_PROTOCOLS , value: "{\"TCP;7001\"}" }
    - { regex: LISTENER_START , value: "\"LISTENER_ORACLEDB1\"" }
    - { regex: INSTALLED_COMPONENTS , value: "{\"server\",\"client\",\"net8\",\"javavm\"}" }

- name: disable keys for netca.rsp (common)
  become: yes
  lineinfile: 
    path: "{{ oracle_tmp }}/netca.rsp"
    regexp: "^{{ item.regex }}"
    line: "#{{ item.regex }}"
  with_items:
    - { regex: NAMING_METHODS }
    - { regex: NSN_NUMBER }
    - { regex: NSN_NAMES }
    - { regex: NSN_SERVICE }
    - { regex: NSN_PROTOCOLS }
    

- name: set inline changes for db_install.rsp (common)
  become: yes
  lineinfile: 
    path: "{{ oracle_tmp }}/db_install.rsp"
    regexp: "^{{ item.regex }}"
    line: "{{ item.regex }}={{ item.value }}"
  with_items:
    - { regex: oracle.install.option, value: "{{ install_mode }}" }
    - { regex: ORACLE_HOSTNAME, value: "{{ ansible_hostname }}" }
    - { regex: UNIX_GROUP_NAME, value: "{{ oracle_group[0].name }}" }
    - { regex: INVENTORY_LOCATION, value: "{{ oracle_base }}/oraInventory" }
    - { regex: SELECTED_LANGUAGES, value: "en" }
    - { regex: ORACLE_HOME, value: "{{ oracle_home }}" }
    - { regex: ORACLE_BASE, value: "{{ oracle_base }}" }
    - { regex: oracle.install.db.InstallEdition, value: "{{ oracle_edition }}" }
  #  - { regex: oracle.install.db.isCustomInstall, value: "false" }
    - { regex: oracle.install.db.DBA_GROUP, value: "{{ oracle_dba_group[0].name }}" }
    - { regex: oracle.install.db.OPER_GROUP, value: "{{ oracle_dba_group[0].name }}" }
    - { regex: oracle.install.db.config.starterdb.type, value: "GENERAL_PURPOSE" }
    - { regex: oracle.install.db.config.starterdb.globalDBName, value: "{{ oracle_db_name }}" }
    - { regex: oracle.install.db.config.starterdb.SID, value: "{{ oracle_db_name }}" }
    - { regex: oracle.install.db.config.starterdb.characterSet, value: "AL32UTF8" }
    - { regex: oracle.install.db.config.starterdb.memoryLimit, value: "{{ oracle_db_mem }}" }
    - { regex: oracle.install.db.config.starterdb.memoryOption, value: "true" }
    - { regex: oracle.install.db.config.starterdb.installExampleSchemas, value: "false" }
    - { regex: oracle.install.db.config.starterdb.password.ALL, value: "{{ oracle_db_syspass }}" }
    - { regex: oracle.install.db.config.starterdb.storageType, value: "{{ oracle_storage_type }}" }
    - { regex: oracle.install.db.config.starterdb.fileSystemStorage.dataLocation, value: "{{ oracle_base }}/data" }
    - { regex: SECURITY_UPDATES_VIA_MYORACLESUPPORT, value: "false" }
    - { regex: DECLINE_SECURITY_UPDATES, value: "true" }

- name: set inline changes (12.1c)
  become: yes
  lineinfile: 
    path: /tmp/oracle/db_install.rsp
    regexp: "^{{ item.regex }}"
    line: "{{ item.regex }}={{ item.value }}"
  with_items:
    - { regex: oracle.install.db.BACKUPDBA_GROUP, value: "{{ oracle_dba_group[0].name }}" }
    - { regex: oracle.install.db.DGDBA_GROUP, value: "{{ oracle_dba_group[0].name }}" }
    - { regex: oracle.install.db.KMDBA_GROUP, value: "{{ oracle_dba_group[0].name }}" }
  when: oracle_version | regex_replace ('^([0-9]{1,2}.[0-9]{1,}.[0-9]{1,})(.*)$','\g<1>') == "12.1.0"

- name: set inline changes (12.2c)
  become: yes
  lineinfile: 
    path: /tmp/oracle/db_install.rsp
    regexp: "^{{ item.regex }}"
    line: "{{ item.regex }}={{ item.value }}"
  with_items:
    - { regex: oracle.install.db.OSDBA_GROUP, value: "{{ oracle_dba_group[0].name }}" }
    - { regex: oracle.install.db.OSOPER_GROUP, value: "{{ oracle_dba_group[0].name }}" }
    - { regex: oracle.install.db.OSBACKUPDBA_GROUP, value: "{{ oracle_dba_group[0].name }}" }
    - { regex: oracle.install.db.OSDGDBA_GROUP, value: "{{ oracle_dba_group[0].name }}" }
    - { regex: oracle.install.db.OSKMDBA_GROUP, value: "{{ oracle_dba_group[0].name }}" }
    - { regex: oracle.install.db.OSRACDBA_GROUP, value: "{{ oracle_dba_group[0].name }}" }
  when: oracle_version | regex_replace ('^([0-9]{1,2}.[0-9]{1,}.[0-9]{1,})(.*)$','\g<1>') == "12.2.0"
